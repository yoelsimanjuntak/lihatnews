<?php
$rterkini = $this->db
->join(TBL__POSTCATEGORIES,TBL__POSTCATEGORIES.'.'.COL_POSTCATEGORYID." = ".TBL__POSTS.".".COL_POSTCATEGORYID,"inner")
->order_by(COL_POSTDATE, 'desc')
->limit(11)
->get(TBL__POSTS)
->result_array();

$qpopular = @"
select * from (
select *, (select count(*) from logs where logs.PostID = p.PostID) as HitCount
from _posts p
) tbl
order by tbl.HitCount desc, tbl.PostDate desc
limit 10
";
$rpopular = $this->db->query($qpopular)->result_array();

$rsepekan = $this->db
->join(TBL__POSTCATEGORIES,TBL__POSTCATEGORIES.'.'.COL_POSTCATEGORYID." = ".TBL__POSTS.".".COL_POSTCATEGORYID,"inner")
->order_by(COL_POSTDATE, 'desc')
->limit(20)
->get(TBL__POSTS)
->result_array();
?>
<section class="home">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-sm-12 col-xs-12">
        <?php
        if(!empty($rterkini)) {
          ?>
          <div class="headline">
            <div class="nav" id="headline-nav">
              <a class="left carousel-control" role="button" data-slide="prev">
                <span class="ion-ios-arrow-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="right carousel-control" role="button" data-slide="next">
                <span class="ion-ios-arrow-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>
            <div class="owl-carousel owl-theme" id="headline">
              <?php
              foreach($rterkini as $r) {
                ?>
                <div class="item">
                  <a href="<?=site_url('site/home/page/'.$r[COL_POSTSLUG])?>">
                    <!--<div class="badge">BARU!</div>--><?=$r[COL_POSTTITLE]?>
                  </a>
                </div>
                <?php
              }
              ?>
            </div>
          </div>
          <?php
        }
        ?>
        <div class="owl-carousel owl-theme slide" id="featured" >
          <?php
          if(!empty($rterkini)) {
            $postID = $rterkini[0][COL_POSTID];
            $rthumbnail = $this->db
            ->where(COL_ISTHUMBNAIL, 1)
            ->where(COL_POSTID, $postID)
            ->get(TBL__POSTIMAGES)
            ->row_array();

            ?>
            <div class="item">
              <article class="featured">
                <div class="overlay"></div>
                <figure>
                  <img src="<?=MY_UPLOADURL.$rthumbnail[COL_IMGPATH]?>" alt="<?=$rterkini[0][COL_POSTTITLE]?>">
                </figure>
                <div class="details">
                  <div class="category"><a href="<?=site_url('site/home/search').'?cat='.$rterkini[0][COL_POSTCATEGORYID]?>"><?=$rterkini[0][COL_POSTCATEGORYNAME]?></a></div>
                  <h1><a href="<?=site_url('site/home/page/'.$rterkini[0][COL_POSTSLUG])?>"><?=$rterkini[0][COL_POSTTITLE]?></a></h1>
                  <div class="time"><?=date("d-m-Y", strtotime($rterkini[0][COL_POSTDATE]))?></div>
                </div>
              </article>
            </div>
            <?php
          }
          ?>

        </div>
        <?php
        if(!empty($rterkini) && count($rterkini)>1) {
          ?>
          <div class="line">
            <div>Berita Terkini</div>
          </div>
          <div class="row">
            <?php
            for($i=1; $i<count($rterkini); $i++) {
              $r=$rterkini[$i];
              $postID = $r[COL_POSTID];
              $rthumbnail = $this->db
              ->where(COL_ISTHUMBNAIL, 1)
              ->where(COL_POSTID, $postID)
              ->get(TBL__POSTIMAGES)
              ->row_array();
              $rhitcount = $this->db->query("select count(*) as COUNT from logs where PostID=$postID")->row_array();

              $strippedcontent = htmlspecialchars_decode(strip_tags($r[COL_POSTCONTENT]));
              ?>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="row">
                  <article class="article col-md-12">
                    <div class="inner">
                      <?php
                      if(!empty($rthumbnail) && file_exists(MY_UPLOADPATH.$rthumbnail[COL_IMGPATH])) {
                        ?>
                        <figure>
                          <a href="<?=site_url('site/home/page/'.$r[COL_POSTSLUG])?>">
                            <img src="<?=MY_UPLOADURL.$rthumbnail[COL_IMGPATH]?>" alt="<?=$r[COL_POSTTITLE]?>">
                          </a>
                        </figure>
                        <?php
                      }
                      ?>
                      <div class="padding">
                        <div class="detail">
                          <div class="time"><?=date("d-m-Y", strtotime($r[COL_POSTDATE]))?></div>
                          <div class="category"><a href="<?=site_url('site/home/search').'?cat='.$r[COL_POSTCATEGORYID]?>"><?=$r[COL_POSTCATEGORYNAME]?></a></div>
                        </div>
                        <h2><a href="<?=site_url('site/home/page/'.$r[COL_POSTSLUG])?>"><?=$r[COL_POSTTITLE]?></a></h2>
                        <p><?=strlen($strippedcontent) > 150 ? substr($strippedcontent, 0, 150) . "..." : $strippedcontent ?></p>
                        <footer>
                          <!--<a href="#" class="love" style="font-size: 10pt !important"><i class="far fa-eye"></i> <div><?=number_format($rhitcount['COUNT'])?></div></a>-->
                          <a class="btn btn-primary more" href="<?=site_url('site/home/page/'.$r[COL_POSTSLUG])?>">
                            <div><small>SELENGKAPNYA</small></div>
                            <div><i class="ion-ios-arrow-thin-right"></i></div>
                          </a>
                        </footer>
                      </div>
                    </div>
                  </article>

                </div>
              </div>
              <?php
            }
            ?>
          </div>
          <?php
        }
        ?>
      </div>
      <div class="col-xs-6 col-md-4 sidebar" id="sidebar">
        <div class="sidebar-title for-tablet">Sidebar</div>
        <aside>
          <h1 class="aside-title">Terpopuler <!--<a href="#" class="all">SELENGKAPNYA <i class="ion-ios-arrow-right"></i></a>--></h1>
          <div class="aside-body">
            <?php
            foreach($rpopular as $r) {
              $postID = $r[COL_POSTID];
              $rthumbnail = $this->db
              ->where(COL_ISTHUMBNAIL, 1)
              ->where(COL_POSTID, $postID)
              ->get(TBL__POSTIMAGES)
              ->row_array();
              ?>
              <article class="article-mini">
                <div class="inner">
                  <figure>
                    <a href="<?=site_url('site/home/page/'.$r[COL_POSTSLUG])?>">
                      <img src="<?=MY_UPLOADURL.$rthumbnail[COL_IMGPATH]?>" alt="<?=$r[COL_POSTTITLE]?>">
                    </a>
                  </figure>
                  <div class="padding">
                    <h1><a href="<?=site_url('site/home/page/'.$r[COL_POSTSLUG])?>"><?=strlen($r[COL_POSTTITLE]) > 75 ? substr($r[COL_POSTTITLE], 0, 75) . "..." : $r[COL_POSTTITLE] ?></a></h1>
                  </div>
                </div>
              </article>
              <?php
            }
            ?>
          </div>
        </aside>
        <aside>
          <div class="aside-body">
            <form class="newsletter">
              <div class="icon">
                <i class="ion-ios-email-outline"></i>
                <h1>NEWSLETTER</h1>
              </div>
              <div class="input-group">
                <input type="email" class="form-control email" placeholder="Alamat Email">
                <div class="input-group-btn">
                  <button class="btn btn-primary"><i class="ion-paper-airplane"></i></button>
                </div>
              </div>
              <p>Update info berita terkini dengan berlangganan (GRATIS) melalui email anda.</p>
            </form>
          </div>
        </aside>
        <aside style="display: none">
          <ul class="nav nav-tabs nav-justified" role="tablist">
            <li class="active">
              <a href="#recomended" aria-controls="recomended" role="tab" data-toggle="tab">
                <i class="ion-android-star-outline"></i> Recomended
              </a>
            </li>
            <li>
              <a href="#comments" aria-controls="comments" role="tab" data-toggle="tab">
                <i class="ion-ios-chatboxes-outline"></i> Comments
              </a>
            </li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="recomended">
              <article class="article-fw">
                <div class="inner">
                  <figure>
                    <a href="single.html">
                      <img src="<?=base_url()?>assets/themes/magz/images/news/img16.jpg" alt="Sample Article">
                    </a>
                  </figure>
                  <div class="details">
                    <div class="detail">
                      <div class="time">December 31, 2016</div>
                      <div class="category"><a href="category.html">Sport</a></div>
                    </div>
                    <h1><a href="single.html">Donec congue turpis vitae mauris</a></h1>
                    <p>
                      Donec congue turpis vitae mauris condimentum luctus. Ut dictum neque at egestas convallis.
                    </p>
                  </div>
                </div>
              </article>
              <div class="line"></div>
              <article class="article-mini">
                <div class="inner">
                  <figure>
                    <a href="single.html">
                      <img src="<?=base_url()?>assets/themes/magz/images/news/img05.jpg" alt="Sample Article">
                    </a>
                  </figure>
                  <div class="padding">
                    <h1><a href="single.html">Duis aute irure dolor in reprehenderit in voluptate velit</a></h1>
                    <div class="detail">
                      <div class="category"><a href="category.html">Lifestyle</a></div>
                      <div class="time">December 22, 2016</div>
                    </div>
                  </div>
                </div>
              </article>
              <article class="article-mini">
                <div class="inner">
                  <figure>
                    <a href="single.html">
                      <img src="<?=base_url()?>assets/themes/magz/images/news/img02.jpg" alt="Sample Article">
                    </a>
                  </figure>
                  <div class="padding">
                    <h1><a href="single.html">Fusce ullamcorper elit at felis cursus suscipit</a></h1>
                    <div class="detail">
                      <div class="category"><a href="category.html">Travel</a></div>
                      <div class="time">December 21, 2016</div>
                    </div>
                  </div>
                </div>
              </article>
              <article class="article-mini">
                <div class="inner">
                  <figure>
                    <a href="single.html">
                      <img src="<?=base_url()?>assets/themes/magz/images/news/img10.jpg" alt="Sample Article">
                    </a>
                  </figure>
                  <div class="padding">
                    <h1><a href="single.html">Duis aute irure dolor in reprehenderit in voluptate velit</a></h1>
                    <div class="detail">
                      <div class="category"><a href="category.html">Healthy</a></div>
                      <div class="time">December 20, 2016</div>
                    </div>
                  </div>
                </div>
              </article>
            </div>
            <div class="tab-pane comments" id="comments">
              <div class="comment-list sm">
                <div class="item">
                  <div class="user">
                    <figure>
                      <img src="<?=base_url()?>assets/themes/magz/images/img01.jpg" alt="User Picture">
                    </figure>
                    <div class="details">
                      <h5 class="name">Mark Otto</h5>
                      <div class="time">24 Hours</div>
                      <div class="description">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                      </div>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <div class="user">
                    <figure>
                      <img src="<?=base_url()?>assets/themes/magz/images/img01.jpg" alt="User Picture">
                    </figure>
                    <div class="details">
                      <h5 class="name">Mark Otto</h5>
                      <div class="time">24 Hours</div>
                      <div class="description">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                      </div>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <div class="user">
                    <figure>
                      <img src="<?=base_url()?>assets/themes/magz/images/img01.jpg" alt="User Picture">
                    </figure>
                    <div class="details">
                      <h5 class="name">Mark Otto</h5>
                      <div class="time">24 Hours</div>
                      <div class="description">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </aside>
        <aside style="display: none">
          <h1 class="aside-title">Videos
            <div class="carousel-nav" id="video-list-nav">
              <div class="prev"><i class="ion-ios-arrow-left"></i></div>
              <div class="next"><i class="ion-ios-arrow-right"></i></div>
            </div>
          </h1>
          <div class="aside-body">
            <ul class="video-list" data-youtube='"carousel":true, "nav": "#video-list-nav"'>
              <li><a data-youtube-id="SBjQ9tuuTJQ" data-action="magnific"></a></li>
              <li><a data-youtube-id="9cVJr3eQfXc" data-action="magnific"></a></li>
              <li><a data-youtube-id="DnGdoEa1tPg" data-action="magnific"></a></li>
            </ul>
          </div>
        </aside>
        <aside id="sponsored">
          <h1 class="aside-title">Pojok Iklan</h1>
          <div class="aside-body">
            <ul class="sponsored">
              <li>
                <a href="#">
                  <img src="<?=base_url()?>assets/themes/magz/images/sponsored.png" alt="Sponsored">
                </a>
              </li>
              <li>
                <a href="#">
                  <img src="<?=base_url()?>assets/themes/magz/images/sponsored.png" alt="Sponsored">
                </a>
              </li>
              <li>
                <a href="#">
                  <img src="<?=base_url()?>assets/themes/magz/images/sponsored.png" alt="Sponsored">
                </a>
              </li>
              <li>
                <a href="#">
                  <img src="<?=base_url()?>assets/themes/magz/images/sponsored.png" alt="Sponsored">
                </a>
              </li>
            </ul>
          </div>
        </aside>
      </div>
    </div>
  </div>
</section>

<section class="best-of-the-week">
  <div class="container">
    <h1><div class="text">BERITA DALAM SEPEKAN</div>
      <div class="carousel-nav" id="best-of-the-week-nav">
        <div class="prev">
          <i class="ion-ios-arrow-left"></i>
        </div>
        <div class="next">
          <i class="ion-ios-arrow-right"></i>
        </div>
      </div>
    </h1>
    <div class="owl-carousel owl-theme carousel-1">
      <?php
      foreach($rsepekan as $r) {
        $postID = $r[COL_POSTID];
        $rthumbnail = $this->db
        ->where(COL_ISTHUMBNAIL, 1)
        ->where(COL_POSTID, $postID)
        ->get(TBL__POSTIMAGES)
        ->row_array();

        $strippedcontent = htmlspecialchars_decode(strip_tags($r[COL_POSTCONTENT]));
        ?>
        <article class="article">
          <div class="inner">
            <figure>
              <a href="single.html">
                <img src="<?=MY_UPLOADURL.$rthumbnail[COL_IMGPATH]?>" alt="<?=$r[COL_POSTTITLE]?>">
              </a>
            </figure>
            <div class="padding">
              <div class="detail">
                  <div class="time"><?=date("d-m-Y", strtotime($r[COL_POSTDATE]))?></div>
                  <div class="category"><a href="<?=site_url('site/home/search').'?cat='.$r[COL_POSTCATEGORYID]?>"><?=$r[COL_POSTCATEGORYNAME]?></a></div>
              </div>
              <h2><a href="<?=site_url('site/home/page/'.$r[COL_POSTSLUG])?>"><?=$r[COL_POSTTITLE]?></a></h2>
              <!--<p><?=strlen($strippedcontent) > 50 ? substr($strippedcontent, 0, 50) . "..." : $strippedcontent ?></p>-->
            </div>
          </div>
        </article>
        <?php
      }
      ?>

    </div>
  </div>
</section>
