<?php
$qpopular = @"
select * from (
select *, (select count(*) from logs where logs.PostID = p.PostID) as HitCount
from _posts p
) tbl
order by tbl.HitCount desc, tbl.PostDate desc
limit 10
";
$rpopular = $this->db->query($qpopular)->result_array();

$rheader = $this->db
->where(COL_ISHEADER, 1)
->where(COL_POSTID, $data[COL_POSTID])
->get(TBL__POSTIMAGES)
->result_array();

$postContent = $data[COL_POSTCONTENT];
$rimg = $this->db
->where(COL_ISHEADER.' != ', 1)
->where(COL_POSTID, $data[COL_POSTID])
->get(TBL__POSTIMAGES)
->result_array();
foreach($rimg as $img) {
  if(!empty($img[COL_IMGSHORTCODE])) {
    $postContent = str_replace($img[COL_IMGSHORTCODE], '<img src="'.MY_UPLOADURL.$img[COL_IMGPATH].'" style="max-width: 100%" /><br /><small style="font-size: 10px; font-style:italic;">'.$img[COL_IMGDESC].'</small>', $postContent);
  }
}

$arrTags = array();
if(!empty($data[COL_POSTMETATAGS])) {
  $arrTags = explode(",", $data[COL_POSTMETATAGS]);
}

$txtShareWA = 'Jangan lewatkan update berita dan informasi terbaru dari LIHATNEWS - '.urlencode(current_url());
?>
<section class="single">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <article class="article main-article">
          <header>
            <h3><?=$data[COL_POSTTITLE]?></h3>
            <ul class="details">
              <li><?=date("d-m-Y", strtotime($data[COL_POSTDATE]))?></li>
              <li><a><?=$data[COL_POSTCATEGORYNAME]?></a></li>
            </ul>
          </header>
          <div class="main">
            <div class="featured">
              <?php
              foreach($rheader as $r) {
                ?>
                <figure>
                  <img src="<?=MY_UPLOADURL.$r[COL_IMGPATH]?>" />
                  <figcaption><?=$r[COL_IMGDESC]?></figcaption>
                </figure>
                <?php
              }
              ?>

            </div>
            <?=$postContent?>
          </div>
          <?php
          if(!empty($arrTags)) {
            ?>
            <footer>
              <div class="row">
                <div class="col-12">
                  <ul class="tags">
                    <?php
                    foreach($arrTags as $t) {
                      ?>
                      <li><a href="<?=site_url('site/home/search').'?tag='.$t?>"><?=$t?></a></li>
                      <?php
                    }
                    ?>
                  </ul>
                </div>
              </div>
            </footer>
            <?php
          }
          ?>
        </article>
        <div class="sharing">
        <div class="title"><i class="ion-android-share-alt"></i> BAGIKAN MELALUI:</div>
          <ul class="social">
            <li>
              <a class="facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?=current_url()?>" target="_blank">
                <i class="ion-social-facebook"></i> Facebook
              </a>
            </li>
            <li>
              <a href="whatsapp://send?text=<?=$txtShareWA?>" data-action="share/whatsapp/share" style="background: #128C7E; color: #fff">
                <i class="fab fa-whatsapp"></i> Whatsapp
              </a>
            </li>
            <li>
              <a href="#" class="twitter">
                <i class="ion-social-twitter"></i> Twitter
              </a>
            </li>

          </ul>
        </div>
        <div class="line thin"></div>
        <div id="disqus_thread"></div>
      </div>
      <div class="col-md-4 sidebar" id="sidebar">
        <aside id="sponsored">
          <h1 class="aside-title">Pojok Iklan</h1>
          <div class="aside-body">
            <ul class="sponsored">
              <li>
                <a href="#">
                  <img src="<?=base_url()?>assets/themes/magz/images/sponsored.png" alt="Sponsored">
                </a>
              </li>
              <li>
                <a href="#">
                  <img src="<?=base_url()?>assets/themes/magz/images/sponsored.png" alt="Sponsored">
                </a>
              </li>
              <li>
                <a href="#">
                  <img src="<?=base_url()?>assets/themes/magz/images/sponsored.png" alt="Sponsored">
                </a>
              </li>
              <li>
                <a href="#">
                  <img src="<?=base_url()?>assets/themes/magz/images/sponsored.png" alt="Sponsored">
                </a>
              </li>
            </ul>
          </div>
        </aside>
        <aside>
          <h1 class="aside-title">Terpopuler <!--<a href="#" class="all">SELENGKAPNYA <i class="ion-ios-arrow-right"></i></a>--></h1>
          <div class="aside-body">
            <?php
            foreach($rpopular as $r) {
              $postID = $r[COL_POSTID];
              $rthumbnail = $this->db
              ->where(COL_ISTHUMBNAIL, 1)
              ->where(COL_POSTID, $postID)
              ->get(TBL__POSTIMAGES)
              ->row_array();
              ?>
              <article class="article-mini">
                <div class="inner">
                  <figure>
                    <a href="<?=site_url('site/home/page/'.$r[COL_POSTSLUG])?>">
                      <img src="<?=MY_UPLOADURL.$rthumbnail[COL_IMGPATH]?>" alt="<?=$r[COL_POSTTITLE]?>">
                    </a>
                  </figure>
                  <div class="padding">
                    <h1><a href="<?=site_url('site/home/page/'.$r[COL_POSTSLUG])?>"><?=strlen($r[COL_POSTTITLE]) > 75 ? substr($r[COL_POSTTITLE], 0, 75) . "..." : $r[COL_POSTTITLE] ?></a></h1>
                  </div>
                </div>
              </article>
              <?php
            }
            ?>
          </div>
        </aside>
        <aside>
          <div class="aside-body">
            <form class="newsletter">
              <div class="icon">
                <i class="ion-ios-email-outline"></i>
                <h1>NEWSLETTER</h1>
              </div>
              <div class="input-group">
                <input type="email" class="form-control email" placeholder="Alamat Email">
                <div class="input-group-btn">
                  <button class="btn btn-primary"><i class="ion-paper-airplane"></i></button>
                </div>
              </div>
              <p>Update info berita terkini dengan berlangganan (GRATIS) melalui email anda.</p>
            </form>
          </div>
        </aside>
      </div>
    </div>
  </div>
</section>
<script>

    /**
     *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
     *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
    /*
     */
    var disqus_config = function () {
        this.page.url = '<?=site_url('site/home/page/'.$data[COL_POSTSLUG])?>';  // Replace PAGE_URL with your page's canonical URL variable
        this.page.identifier = '<?=$data[COL_POSTSLUG]?>'; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
    };
    (function() { // DON'T EDIT BELOW THIS LINE
        var d = document, s = d.createElement('script');
        s.src = '<?=$this->setting_web_disqus_url?>';
        s.setAttribute('data-timestamp', +new Date());
        (d.head || d.body).appendChild(s);
    })();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
