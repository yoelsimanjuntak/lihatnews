<?php
$rkategori = $this->db
->order_by(COL_SEQ)
->get(TBL__POSTCATEGORIES)
->result_array();

$arrTags = array();
$qtags = @"
select * from _posts p
order by p.PostDate desc
limit 100
";
$rtags = $this->db->query($qtags)->result_array();
foreach($rtags as $r) {
  $tags_ = array();
  if(!empty($r[COL_POSTMETATAGS])) {
    $tags_ = explode(",", $r[COL_POSTMETATAGS]);
  }

  foreach($tags_ as $t) {
    if(array_key_exists($t, $arrTags)) $arrTags[$t]++;
    else $arrTags[$t]=1;
  }
}
arsort($arrTags);
//echo json_encode($arrTags);
//exit();
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
    <title><?=isset($ogtitle)?$ogtitle:(!empty($title) ? $this->setting_web_name.' - '.$title : $this->setting_web_name)?></title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta name="description" content="<?=$this->setting_web_name.' '.$this->setting_web_desc?>">
		<meta name="author" content="Partopi Tao">
		<meta name="keyword" content="lihatnews, lumbung, informasi, harian, aktual, terpercaya">

		<link rel="stylesheet" href="<?=base_url()?>assets/themes/magz/scripts/bootstrap/bootstrap-4.min.css">
		<link rel="stylesheet" href="<?=base_url()?>assets/themes/magz/scripts/bootstrap/bootstrap.min.css">
		<!-- IonIcons -->
		<link rel="stylesheet" href="<?=base_url()?>assets/themes/magz/scripts/ionicons/css/ionicons.min.css">
		<link rel="stylesheet" href="<?=base_url()?>assets/tbs/fontawesome-pro/web/css/all.min.css" />
		<!-- Toast -->
		<link rel="stylesheet" href="<?=base_url()?>assets/themes/magz/scripts/toast/jquery.toast.min.css">
		<!-- OwlCarousel -->
		<link rel="stylesheet" href="<?=base_url()?>assets/themes/magz/scripts/owlcarousel/dist/assets/owl.carousel.min.css">
		<link rel="stylesheet" href="<?=base_url()?>assets/themes/magz/scripts/owlcarousel/dist/assets/owl.theme.default.min.css">
		<!-- Magnific Popup -->
		<link rel="stylesheet" href="<?=base_url()?>assets/themes/magz/scripts/magnific-popup/dist/magnific-popup.css">
		<link rel="stylesheet" href="<?=base_url()?>assets/themes/magz/scripts/sweetalert/dist/sweetalert.css">
		<!-- Custom style -->
		<link rel="stylesheet" href="<?=base_url()?>assets/themes/magz/css/style.css">
		<link rel="stylesheet" href="<?=base_url()?>assets/themes/magz/css/skins/all.css">
		<link rel="stylesheet" href="<?=base_url()?>assets/themes/magz/css/demo.css">

		<!-- JQUERY -->
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/modernizr/modernizr.js"></script>

    <link rel="icon" type="image/png" href=<?=MY_IMAGEURL.$this->setting_web_logo?>>

		<style>
		.se-pre-con {
			position: fixed;
			left: 0px;
			top: 0px;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url(<?=base_url()?>assets/preloader/<?=$this->setting_web_preloader?>) center no-repeat #fff;
		}

		.article h2, .headline .item a, .article-mini h1 {
			text-transform: none !important;
		}
    .skin-orange .featured .details h1 {
      font-size: 2.5rem !important;
    }
		</style>
		<script>
		$(window).load(function() {
			$(".se-pre-con").fadeOut("slow");
		});
		</script>
    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-9585490761913987" crossorigin="anonymous"></script>

    <!-- Shareable -->
		<meta property="og:title" content="<?=isset($ogtitle)?$ogtitle:$this->setting_web_name?>" />
		<meta property="og:type" content="article" />
		<meta property="og:url" content="<?=current_url()?>" />
		<meta property="og:image" content="<?=isset($ogimg)?$ogimg:MY_IMAGEURL.'logo-horizontal-with-bg.png'?>" />
    <?php
    if(isset($ogdesc)) {
      ?>
      <meta property="og:description" content="<?=$ogdesc?>" />
      <?php
    }
    ?>

		<!-- Bootstrap -->
	</head>

	<body class="skin-orange">
		<div class="se-pre-con"></div>
		<header class="primary">
			<div class="firstbar">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-12">
							<div class="brand" style="margin: 0 !important">
								<a href="<?=site_url()?>">
									<img src="<?=MY_IMAGEURL.'logo-horizontal-black.png'?>" alt="Logo" style="max-width: 100% !important; max-height: 100px !important; width: auto !important">
								</a>
							</div>
						</div>
						<div class="col-md-6 col-sm-12">
							<form action="<?=site_url('site/home/search')?>" method="get" class="search" autocomplete="off">
								<div class="form-group">
									<div class="input-group">
										<input type="text" name="q" class="form-control" placeholder="Pencarian">
										<div class="input-group-btn">
											<button class="btn btn-primary"><i class="ion-search"></i></button>
										</div>
									</div>
								</div>
								<?php
								if(!empty($arrTags)) {
									?>
									<div class="help-block">
										<div>Populer:</div>
										<ul>
											<?php
											$ntags = 0;
											foreach($arrTags as $t=>$val) {
												if($ntags>5) break;
												?>
												<li><?=$t?></li>
												<?php
												$ntags++;
											}
											?>
										</ul>
									</div>
									<?php
								}
								?>
							</form>
						</div>
						<!--<div class="col-md-3 col-sm-12 text-right">
							<ul class="nav-icons">
								<li><a href="register.html"><i class="ion-person-add"></i><div>Register</div></a></li>
								<li><a href="login.html"><i class="ion-person"></i><div>Login</div></a></li>
							</ul>
						</div>-->
					</div>
				</div>
			</div>

			<!-- Start nav -->
			<nav class="menu">
				<div class="container">
					<div class="brand">
						<a href="#">
							<img src="<?=MY_IMAGEURL.'logo-horizontal-black.png'?>" alt="Logo">
						</a>
					</div>
					<div class="mobile-toggle">
						<a href="#" data-toggle="menu" data-target="#menu-list"><i class="ion-navicon-round"></i></a>
					</div>
					<div class="mobile-toggle">
						<a href="#" data-toggle="sidebar" data-target="#sidebar"><i class="ion-ios-arrow-left"></i></a>
					</div>
					<div id="menu-list">
						<ul class="nav-list">
							<!--<li class="for-tablet nav-title"><a>Menu</a></li>
							<li class="for-tablet"><a href="login.html">Login</a></li>
							<li class="for-tablet"><a href="register.html">Register</a></li>-->
							<li><a href="<?=site_url()?>">Beranda</a></li>
							<?php
							if(!empty($rkategori)) {
								?>
								<li class="dropdown magz-dropdown">
									<a href="category.html">Kategori <i class="ion-ios-arrow-right"></i></a>
									<ul class="dropdown-menu">
										<?php
										foreach($rkategori as $kat) {
											?>
											<li><a href="<?=site_url('site/home/search').'?cat='.$kat[COL_POSTCATEGORYID]?>"><?=$kat[COL_POSTCATEGORYNAME]?></a></li>
											<?php
										}
										?>
									</ul>
								</li>
								<?php
							}
							?>

							<li><a href="#">Tentang Kami</a></li>
							<li><a href="#">Kontak</a></li>
							<li class="dropdown magz-dropdown" style="display: none !important">
								<a href="category.html">Pages <i class="ion-ios-arrow-right"></i></a>
								<ul class="dropdown-menu">
									<li><a href="index.html">Home</a></li>
									<li class="dropdown magz-dropdown">
										<a href="#">Authentication <i class="ion-ios-arrow-right"></i></a>
										<ul class="dropdown-menu">
											<li><a href="login.html">Login</a></li>
											<li><a href="register.html">Register</a></li>
											<li><a href="forgot.html">Forgot Password</a></li>
											<li><a href="reset.html">Reset Password</a></li>
										</ul>
									</li>
									<li><a href="category.html">Category</a></li>
									<li><a href="single.html">Single</a></li>
									<li><a href="page.html">Page</a></li>
									<li><a href="search.html">Search</a></li>
									<li><a href="contact.html">Contact</a></li>
									<li class="dropdown magz-dropdown">
										<a href="#">Error <i class="ion-ios-arrow-right"></i></a>
										<ul class="dropdown-menu">
											<li><a href="403.html">403</a></li>
											<li><a href="404.html">404</a></li>
											<li><a href="500.html">500</a></li>
											<li><a href="503.html">503</a></li>
										</ul>
									</li>
								</ul>
							</li>
							<li class="dropdown magz-dropdown" style="display: none !important"><a href="#">Dropdown <i class="ion-ios-arrow-right"></i></a>
								<ul class="dropdown-menu">
									<li><a href="category.html">Internet</a></li>
									<li class="dropdown magz-dropdown"><a href="category.html">Troubleshoot <i class="ion-ios-arrow-right"></i></a>
										<ul class="dropdown-menu">
											<li><a href="category.html">Software</a></li>
											<li class="dropdown magz-dropdown"><a href="category.html">Hardware <i class="ion-ios-arrow-right"></i></a>
												<ul class="dropdown-menu">
													<li><a href="category.html">Main Board</a></li>
													<li><a href="category.html">RAM</a></li>
													<li><a href="category.html">Power Supply</a></li>
												</ul>
											</li>
											<li><a href="category.html">Brainware</a>
										</ul>
									</li>
									<li><a href="category.html">Office</a></li>
									<li class="dropdown magz-dropdown"><a href="#">Programming <i class="ion-ios-arrow-right"></i></a>
										<ul class="dropdown-menu">
											<li><a href="category.html">Web</a></li>
											<li class="dropdown magz-dropdown"><a href="category.html">Mobile <i class="ion-ios-arrow-right"></i></a>
												<ul class="dropdown-menu">
													<li class="dropdown magz-dropdown"><a href="category.html">Hybrid <i class="ion-ios-arrow-right"></i></a>
														<ul class="dropdown-menu">
															<li><a href="#">Ionic Framework 1</a></li>
															<li><a href="#">Ionic Framework 2</a></li>
															<li><a href="#">Ionic Framework 3</a></li>
															<li><a href="#">Framework 7</a></li>
														</ul>
													</li>
													<li><a href="category.html">Native</a></li>
												</ul>
											</li>
											<li><a href="category.html">Desktop</a></li>
										</ul>
									</li>
								</ul>
							</li>
							<li class="dropdown magz-dropdown magz-dropdown-megamenu" style="display: none !important"><a href="#">Mega Menu <i class="ion-ios-arrow-right"></i> <div class="badge">Hot</div></a>
								<div class="dropdown-menu megamenu">
									<div class="megamenu-inner">
										<div class="row">
											<div class="col-md-3">
												<div class="row">
													<div class="col-md-12">
														<h2 class="megamenu-title">Trending</h2>
													</div>
												</div>
												<ul class="vertical-menu">
													<li><a href="#"><i class="ion-ios-circle-outline"></i> Mega menu is a new feature</a></li>
													<li><a href="#"><i class="ion-ios-circle-outline"></i> This is an example</a></li>
													<li><a href="#"><i class="ion-ios-circle-outline"></i> For a submenu item</a></li>
													<li><a href="#"><i class="ion-ios-circle-outline"></i> You can add</a></li>
													<li><a href="#"><i class="ion-ios-circle-outline"></i> Your own items</a></li>
												</ul>
											</div>
											<div class="col-md-9">
												<div class="row">
													<div class="col-md-12">
														<h2 class="megamenu-title">Featured Posts</h2>
													</div>
												</div>
												<div class="row">
													<article class="article col-md-4 mini">
														<div class="inner">
															<figure>
																<a href="single.html">
																	<img src="<?=base_url()?>assets/themes/magz/images/news/img10.jpg" alt="Sample Article">
																</a>
															</figure>
															<div class="padding">
																<div class="detail">
																	<div class="time">December 10, 2016</div>
																	<div class="category"><a href="category.html">Healthy</a></div>
																</div>
																<h2><a href="single.html">Duis aute irure dolor in reprehenderit in voluptate</a></h2>
															</div>
														</div>
													</article>
													<article class="article col-md-4 mini">
														<div class="inner">
															<figure>
																<a href="single.html">
																	<img src="<?=base_url()?>assets/themes/magz/images/news/img11.jpg" alt="Sample Article">
																</a>
															</figure>
															<div class="padding">
																<div class="detail">
																	<div class="time">December 13, 2016</div>
																	<div class="category"><a href="category.html">Lifestyle</a></div>
																</div>
																<h2><a href="single.html">Duis aute irure dolor in reprehenderit in voluptate</a></h2>
															</div>
														</div>
													</article>
													<article class="article col-md-4 mini">
														<div class="inner">
															<figure>
																<a href="single.html">
																	<img src="<?=base_url()?>assets/themes/magz/images/news/img14.jpg" alt="Sample Article">
																</a>
															</figure>
															<div class="padding">
																<div class="detail">
																	<div class="time">December 14, 2016</div>
																	<div class="category"><a href="category.html">Travel</a></div>
																</div>
																<h2><a href="single.html">Duis aute irure dolor in reprehenderit in voluptate</a></h2>
															</div>
														</div>
													</article>
												</div>
											</div>
										</div>
									</div>
								</div>
							</li>
							<li class="dropdown magz-dropdown magz-dropdown-megamenu"  style="display: none !important"><a href="#">Column <i class="ion-ios-arrow-right"></i></a>
								<div class="dropdown-menu megamenu">
									<div class="megamenu-inner">
										<div class="row">
											<div class="col-md-3">
												<h2 class="megamenu-title">Column 1</h2>
												<ul class="vertical-menu">
													<li><a href="#">Example 1</a></li>
													<li><a href="#">Example 2</a></li>
													<li><a href="#">Example 3</a></li>
													<li><a href="#">Example 4</a></li>
													<li><a href="#">Example 5</a></li>
												</ul>
											</div>
											<div class="col-md-3">
												<h2 class="megamenu-title">Column 2</h2>
												<ul class="vertical-menu">
													<li><a href="#">Example 6</a></li>
													<li><a href="#">Example 7</a></li>
													<li><a href="#">Example 8</a></li>
													<li><a href="#">Example 9</a></li>
													<li><a href="#">Example 10</a></li>
												</ul>
											</div>
											<div class="col-md-3">
												<h2 class="megamenu-title">Column 3</h2>
												<ul class="vertical-menu">
													<li><a href="#">Example 11</a></li>
													<li><a href="#">Example 12</a></li>
													<li><a href="#">Example 13</a></li>
													<li><a href="#">Example 14</a></li>
													<li><a href="#">Example 15</a></li>
												</ul>
											</div>
											<div class="col-md-3">
												<h2 class="megamenu-title">Column 4</h2>
												<ul class="vertical-menu">
													<li><a href="#">Example 16</a></li>
													<li><a href="#">Example 17</a></li>
													<li><a href="#">Example 18</a></li>
													<li><a href="#">Example 19</a></li>
													<li><a href="#">Example 20</a></li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</li>
							<li class="dropdown magz-dropdown" style="display: none !important"><a href="#">Dropdown Icons <i class="ion-ios-arrow-right"></i></a>
								<ul class="dropdown-menu">
									<li><a href="#"><i class="icon ion-person"></i> My Account</a></li>
									<li><a href="#"><i class="icon ion-heart"></i> Favorite</a></li>
									<li><a href="#"><i class="icon ion-chatbox"></i> Comments</a></li>
									<li><a href="#"><i class="icon ion-key"></i> Change Password</a></li>
									<li><a href="#"><i class="icon ion-settings"></i> Settings</a></li>
									<li class="divider"></li>
									<li><a href="#"><i class="icon ion-log-out"></i> Logout</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</nav>
			<!-- End nav -->
		</header>

		<?=$content?>

		<!-- Start footer -->
		<footer class="footer">
			<div class="container">
				<div class="row">
					<div class="col-md-5 col-sm-5 col-xs-12">
						<div class="block">
							<h1 class="block-title" style="color: #FC624D !important">Tentang Kami</h1>
							<div class="block-body">
								<figure class="foot-logo" style="width: 100% !important">
									<img src="<?=MY_IMAGEURL.'logo-horizontal-white.png'?>" class="img-responsive" alt="Logo">
								</figure>
								<p class="brand-description" style="text-align: justify">
									Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
								</p>
								<!--<a href="page.html" class="btn btn-magz white">About Us <i class="ion-ios-arrow-thin-right"></i></a>-->
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<div class="block">
							<h1 class="block-title" style="color: #FC624D !important">Newsletter</h1>
							<div class="block-body">
								<p>Update info berita terkini dengan berlangganan (GRATIS) melalui email anda.</p>
								<form class="newsletter">
									<div class="input-group">
										<div class="input-group-addon">
											<i class="ion-ios-email-outline"></i>
										</div>
										<input type="email" class="form-control email" placeholder="Alamat Email">
									</div>
									<button class="btn btn-primary btn-block white">Berlangganan</button>
								</form>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-xs-12 col-sm-3">
						<div class="block">
							<h1 class="block-title" style="color: #FC624D !important">Tautan</h1>
							<div class="block-body">
								<p>Ikuti dan jelajahi platform media sosial kami.</p>
								<ul class="social trp">
									<li>
										<a href="#" class="facebook">
											<svg><rect width="0" height="0"/></svg>
											<i class="ion-social-facebook"></i>
										</a>
									</li>
									<li>
										<a href="#" class="twitter">
											<svg><rect width="0" height="0"/></svg>
											<i class="ion-social-twitter-outline"></i>
										</a>
									</li>
									<li>
										<a href="#" class="youtube">
											<svg><rect width="0" height="0"/></svg>
											<i class="ion-social-youtube-outline"></i>
										</a>
									</li>
									<li>
										<a href="#" class="instagram">
											<svg><rect width="0" height="0"/></svg>
											<i class="ion-social-instagram-outline"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="copyright">
							COPYRIGHT &copy; <?=$this->setting_web_name?> <?=date('Y')?>. ALL RIGHT RESERVED.
						</div>
					</div>
				</div>
			</div>
		</footer>
		<!-- End Footer -->

		<!-- JS -->
		<script src="<?=base_url()?>assets/themes/magz/js/jquery.js"></script>
		<script src="<?=base_url()?>assets/themes/magz/js/jquery.migrate.js"></script>
		<script src="<?=base_url()?>assets/themes/magz/scripts/bootstrap/bootstrap.min.js"></script>
		<script>var $target_end=$(".best-of-the-week");</script>
		<script src="<?=base_url()?>assets/themes/magz/scripts/jquery-number/jquery.number.min.js"></script>
		<script src="<?=base_url()?>assets/themes/magz/scripts/owlcarousel/dist/owl.carousel.min.js"></script>
		<script src="<?=base_url()?>assets/themes/magz/scripts/magnific-popup/dist/jquery.magnific-popup.min.js"></script>
		<script src="<?=base_url()?>assets/themes/magz/scripts/easescroll/jquery.easeScroll.js"></script>
		<script src="<?=base_url()?>assets/themes/magz/scripts/sweetalert/dist/sweetalert.min.js"></script>
		<script src="<?=base_url()?>assets/themes/magz/scripts/toast/jquery.toast.min.js"></script>
		<!--<script src="<?=base_url()?>assets/themes/magz/js/demo.js"></script>-->
		<script src="<?=base_url()?>assets/themes/magz/js/e-magz.js"></script>
	</body>
</html>
