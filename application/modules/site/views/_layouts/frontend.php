
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title><?=!empty($title) ? $title.' | '.$this->setting_web_name : $this->setting_web_name?></title>

    <!-- Font Awesome Icons -->
    <!--<link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/fontawesome-free/css/all.min.css">-->
    <link rel="stylesheet" href="<?=base_url()?>assets/tbs/css/font-awesome.min.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets/tbs/fontawesome-pro/web/css/all.min.css" />

    <!-- Theme style -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/dist/css/adminlte.min.css">

    <!-- Ionicons -->
    <link href="<?=base_url()?>assets/tbs/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <link rel="icon" type="image/png" href="<?=MY_IMAGEURL.$this->setting_web_logo?>" />

    <!-- Select 2 -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

    <!-- JQUERY -->
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/modernizr/modernizr.js"></script>
    <script src="<?=base_url()?>assets/themes/adminlte-new/dist/js/adminlte.min.js"></script>
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- DataTables -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/datatable/media/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/datatables-bs4/css/dataTables.bootstrap4.css">

    <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/jquery.dataTables.min.js?ver=1"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/dataTables.bootstrap.min.js"></script>
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>


    <!-- datatable reorder _ buttons ext + resp + print -->
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/ColReorderWithResize.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.print.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.print.min.js"></script>
    <link href="<?=base_url()?>assets/datatable/ext/buttons/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/datatable/ext/responsive/css/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/jszip/jszip.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/pdfmake/build/pdfmake.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/pdfmake/build/vfs_fonts.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/responsive/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.html5.min.js"></script>

    <!-- Toastr -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.css">
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.js"></script>

    <!-- daterange picker -->
    <script src="<?=base_url()?>assets/js/moment.js"></script>
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/daterangepicker/daterangepicker.css">

    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/ekko-lightbox/ekko-lightbox.css">

    <script type="text/javascript" src="<?=base_url()?>assets/js/jquery.number.js"></script>

    <script type="text/javascript" src="<?=base_url()?>assets/template/js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/template/js/function.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/template/js/jquery.form.js"></script>

    <!-- my css -->
    <!--<link rel="stylesheet" href="<?=base_url()?>assets/css/styles.css">-->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/my.css">

    <script>
    function startTime() {
      /*$.get('<?=site_url('site/ajax/now')?>', function(data) {
        var res = JSON.parse(data);
        $('#datetime').html(res.Day.toUpperCase()+', '+res.Date+' '+res.Month.toUpperCase()+' '+res.Year+' <span class="text-danger">'+res.Hour+':'+res.Minute+':'+res.Second+'</span>');
      });*/
      var today = new Date();
      $('#datetime').html(moment(today).format('DD')+' '+moment(today).format('MMMM').toUpperCase()+' '+moment(today).format('Y')+' <span style="color: #0000a4">'+moment(today).format('hh')+':'+moment(today).format('mm')+':'+moment(today).format('ss')+'</span>');
      var t = setTimeout(startTime, 1000);
    }
    $(document).ready(function() {
      startTime();
      toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
      };
    });
    $(window).load(function() {
        // Animate loader off screen
        $(".se-pre-con").fadeOut("slow");
    });
    </script>
    <style>
    #footer-section::after {
      background-image: url('<?=MY_IMAGEURL.'hero-bg.png'?>');
      background-size: cover;
      background-position-y: bottom;
      content: "";
      height: 100%;
      left: 0;
      /*opacity: 0.1;*/
      position: absolute;
      top: 0;
      width: 100%;
      z-index: -1;
    }
    #footer-section table td {
      border-top: none !important;
    }
    .se-pre-con {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url(<?=base_url()?>assets/preloader/images/<?=$this->setting_web_preloader?>) center no-repeat #fff;
    }
    </style>
</head>
<body class="hold-transition layout-top-nav layout-navbar-fixed">
  <div class="se-pre-con"></div>
  <div class="wrapper">
    <nav class="main-header navbar navbar-danger navbar-dark" style="border-bottom: none !important">
      <div class="container">
          <a href="<?=site_url()?>" class="navbar-brand">
              <img src="<?=MY_IMAGEURL.$this->setting_web_logo?>" alt="Logo" class="brand-image elevation" style="opacity: .8; width: 40px">
              <!--<span class="brand-text font-weight-bold"><?=$this->setting_web_name?></span>-->
          </a>
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="true" style="background: transparent; border: transparent; color: #f4e4ba">
            <span class="sr-only">Toggle navigation</span>
            <i class="fa fa-bars" aria-hidden="true"></i>
          </button>
          <div class="navbar-collapse collapse text-center pl-0 pt-2" id="navbar-collapse">
            <ul class="navbar-nav">
              <li class="nav-item d-sm-inline-block">
                  <a href="<?=site_url()?>" class="nav-link">BERANDA</a>
              </li>
              <li class="nav-item d-sm-inline-block">
                  <a href="<?=site_url('site/home/post/1')?>" class="nav-link">BERITA</a>
              </li>
              <li class="nav-item d-sm-inline-block">
                  <a href="<?=site_url('site/home/post/3')?>" class="nav-link">GALERI</a>
              </li>
            </ul>
          </div>
      </div>
    </nav>
    <?=$content?>
    <div id="footer-section" class="bg-danger pt-3" style="position: relative; z-index:9; border-top: 1px solid #dee2e6;">
      <div class="content text-white" style="padding: 0 .5rem">
        <div class="container">
          <div class="row">
            <div class="col-lg-3 p-2">
              <div class="row">
                <div class="col-lg-4 p-0">
                  <p class="text-left">
                    <img class="mb-3" src="<?=MY_IMAGEURL.$this->setting_web_logo?>" style="max-width: 100px !important" />
                  </p>
                </div>
                <div class="col-lg-8 p-0">
                  <span class="font-weight-bold" style="color: #FFFF02 !important">
                    <?=nl2br($this->setting_org_name)?>
                  </span>
                  <!--<span>
                    <?=nl2br(GetSetting('SETTING_ORG_REGION'))?>
                  </span>-->
                </div>
                <div class="col-lg-12">
                  <div class="row">
                    <p>
                      <span class="font-weight-bold m-0" style="color: #FFFF02 !important">Alamat: </span><br />
                      <span><?=$this->setting_org_address?></span><br /><br />
                      <span class="font-weight-bold m-0" style="color: #FFFF02 !important">Telp / Fax :</span><br />
                      <span><?=$this->setting_org_phone?> / <?=$this->setting_org_fax?></span><br /><br />
                      <span class="font-weight-bold m-0" style="color: #FFFF02 !important">Email :</span><br />
                      <span><?=$this->setting_org_mail?></span>
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-5 p-2">
              <form action="#">
                <div class="col-lg-12">
                  <h6 class="font-weight-bold" style="color: #FFFF02 !important">KOTAK PESAN</h6>
                  <div class="form-group row">
                    <div class="col-lg-12">
                      <input type="text" class="form-control" placeholder="Nama Lengkap" />
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="No. Telp" />
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <input type="email" class="form-control" placeholder="Email" />
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-lg-12">
                      <textarea class="form-control" rows="3" placeholder="Pesan"></textarea>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-lg-12 text-center">
                      <button type="submit" class="btn btn-block font-weight-bold" style="background-color: #FFFF02; color: #000 !important"><i class="far fa-paper-plane"></i>&nbsp;KIRIM</button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <div class="col-lg-4 p-2">
              <h6 class="font-weight-bold" style="color: #FFFF02 !important">LINK TERKAIT</h6>
              <div class="links d-block">
                <ul>
                  <li><a href="https://batubarakab.go.id/" target="_blank">Website Pemerintah Kabupaten Batubara</a></li>
                </ul>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
    </div>
    <footer class="main-footer bg-danger" style="border-top: none !important;">
        <div class="float-right d-none d-sm-inline">
          Version <b><?=$this->setting_web_version?></b>
        </div>
        <strong>Copyright &copy; <?=date("Y")?> <?=$this->setting_web_name?></strong>. Strongly developed by <b>Partopi Tao</b>.
    </footer>
  </div>
  <script type="text/javascript" src="<?=base_url()?>assets/js/jquery.motio.min.js"></script>
  <script type="text/javascript">
  $(document).ready(function() {
      /*var element = document.querySelector('.content-wrapper');
      var panning = new Motio(element, {
        fps: 30,
        speedX: 30
      });
      panning.play();*/
  });
  </script>
</body>
</html>
