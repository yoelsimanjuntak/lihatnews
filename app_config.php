<?php
$dbpconnect = TRUE;

//MY_CONSTANT
define('MY_BASEURL', 'http://localhost/web-lihatnews');
define('MY_DBHOST', 'localhost');
define('MY_DBUSER', 'root');
define('MY_DBPASS', '');
define('MY_DBNAME', 'web_lihatnews');
define('MY_DBPCONNECT', $dbpconnect);
define('MY_ASSETPATH', './assets');
define('MY_ASSETURL', MY_BASEURL.'/assets');
define('MY_IMAGEPATH', './assets/media/image/');
define('MY_IMAGEURL', MY_ASSETURL.'/media/image/');
define('MY_UPLOADPATH', './assets/media/upload/');
define('MY_UPLOADURL', MY_ASSETURL.'/media/upload/');
define('MY_NOIMAGEURL', MY_ASSETURL.'/media/image/no-image.png');
define('MY_NODATAURL', MY_ASSETURL.'/media/image/no-data.png');
define('SYSTEMUSERNAME', 'admin');

define("URL_SUFFIX", ".jsp");

define("UPLOAD_ALLOWEDTYPES", "gif|jpg|jpeg|png|doc|docx|txt|pdf");
