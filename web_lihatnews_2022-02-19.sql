# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.38-MariaDB)
# Database: web_lihatnews
# Generation Time: 2022-02-18 19:09:36 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table _postcategories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_postcategories`;

CREATE TABLE `_postcategories` (
  `PostCategoryID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Seq` int(10) DEFAULT NULL,
  `PostCategoryName` varchar(50) NOT NULL,
  `PostCategoryLabel` varchar(50) DEFAULT NULL,
  `IsShowEditor` tinyint(1) NOT NULL DEFAULT '1',
  `IsAllowExternalURL` tinyint(1) NOT NULL DEFAULT '0',
  `IsDocumentOnly` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`PostCategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_postcategories` WRITE;
/*!40000 ALTER TABLE `_postcategories` DISABLE KEYS */;

INSERT INTO `_postcategories` (`PostCategoryID`, `Seq`, `PostCategoryName`, `PostCategoryLabel`, `IsShowEditor`, `IsAllowExternalURL`, `IsDocumentOnly`)
VALUES
	(7,1,'Hukum',NULL,1,0,0),
	(8,2,'Sosial',NULL,1,0,0),
	(9,3,'Politik',NULL,1,0,0),
	(10,4,'Ekonomi',NULL,1,0,0),
	(11,5,'Pendidikan',NULL,1,0,0),
	(12,6,'Kesehatan',NULL,1,0,0),
	(13,7,'Opini',NULL,1,0,0),
	(14,8,'Infografis',NULL,1,0,0);

/*!40000 ALTER TABLE `_postcategories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _postimages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_postimages`;

CREATE TABLE `_postimages` (
  `PostImageID` bigint(10) NOT NULL AUTO_INCREMENT,
  `PostID` bigint(10) NOT NULL,
  `ImgPath` text NOT NULL,
  `ImgDesc` varchar(250) DEFAULT NULL,
  `ImgShortcode` varchar(50) DEFAULT NULL,
  `IsHeader` tinyint(1) NOT NULL DEFAULT '1',
  `IsThumbnail` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`PostImageID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_postimages` WRITE;
/*!40000 ALTER TABLE `_postimages` DISABLE KEYS */;

INSERT INTO `_postimages` (`PostImageID`, `PostID`, `ImgPath`, `ImgDesc`, `ImgShortcode`, `IsHeader`, `IsThumbnail`)
VALUES
	(17,20,'sosialisasi-pengelolaan-persampahan-dan-perayaan-world-cleanup-day-wcd-kota-tebing-tinggi-tahun-2021-1.png','Sosialisasi Pengelolaan Persampahan dan Perayaan World Cleanup Day (WCD) Kota Tebing Tinggi Tahun 2021','',1,1),
	(20,23,'mui-kota-tebing-tinggi-salurkan-10-000-dosis-vaksin-bagi-pelajar-1.png','Wali Kota Tebing Tinggi Umar Zunaidi Hasibuan (kanan) beraudiensi dengan MUI Kota Tebing Tinggi','',1,1),
	(23,6,'walikota-tebing-tinggi-ajak-alim-ulama-aktif-sosialisasi-program-vaksinasi-covid-19-1.png','Walikota Tebing Tinggi Ajak Alim Ulama Aktif Sosialisasi Program Vaksinasi Covid 19','',1,1),
	(26,7,'mui-gelar-vaksinasi-pelajar-di-tebing-tinggi-1.png','MUI Gelar Vaksinasi Pelajar di Tebing Tinggi','',1,1),
	(27,19,'mui-kota-tebing-tinggi-menggalang-dana-bantuan-untuk-korban-banjir-di-kota-tebing-tinggi-1.png','MUI Kota Tebing Tinggi Menggalang Dana Bantuan untuk Korban Banjir di Kota Tebing Tinggi','',1,1),
	(28,24,'test-1.png','DEV','',1,1);

/*!40000 ALTER TABLE `_postimages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_posts`;

CREATE TABLE `_posts` (
  `PostID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `PostCategoryID` int(10) NOT NULL,
  `PostDate` date NOT NULL,
  `PostTitle` varchar(200) NOT NULL,
  `PostSlug` varchar(200) NOT NULL,
  `PostContent` longtext,
  `PostMetaTags` text,
  `IsSuspend` tinyint(1) NOT NULL DEFAULT '1',
  `IsRunningText` tinyint(1) NOT NULL DEFAULT '1',
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT '',
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`PostID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_posts` WRITE;
/*!40000 ALTER TABLE `_posts` DISABLE KEYS */;

INSERT INTO `_posts` (`PostID`, `PostCategoryID`, `PostDate`, `PostTitle`, `PostSlug`, `PostContent`, `PostMetaTags`, `IsSuspend`, `IsRunningText`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(6,12,'2021-11-09','Walikota Tebing Tinggi Ajak Alim Ulama Aktif Sosialisasi Program Vaksinasi Covid 19','walikota-tebing-tinggi-ajak-alim-ulama-aktif-sosialisasi-program-vaksinasi-covid-19','<p style=\"text-align:justify\">Walikota Tebingtinggi Ir. H.Umar Zunaidi Hasibuan, MM mengajak alim ulama, tokoh agama di Kota Tebingtinggi aktif sosialisasikan program vaksinasi Covid-19. Hal ini disampaikan Walikota saat penyerahan bantuan alat kesehatan (alkes) dan vaksinasi bagi 450 pelajar diselenggarakan Majelis Ulama Indonesia (MUI) Kota Tebingtinggi bersumber dari Pusat Krisis Kesehatan Kementerian Kesehatan (Kemenkes) RI, Senin&nbsp;(08/11/2021) di halaman Kejaksaan Negeri Tebing Tinggi.</p>\r\n\r\n<p style=\"text-align:justify\">Pada kesempatan itu, Wali Kota berharap MUI Kota Tebing Tinggi agar menyampaikan kepada alim ulama dan tokoh agama agar memberi keyakinan terkait vaksinasi dan penerapan protokol kesehatan COVID-19 kepada masyarakat.</p>\r\n\r\n<p style=\"text-align:justify\">&ldquo;Perlu kiranya MUI menyampaikan himbauan kepada para ustad dan ulama untuk terus disampaikan kepada ummat tentang vaksinasi karena sebagai upaya untuk kesehatan dan keselamatan masyarakat,&rdquo; ujar Wali Kota.</p>\r\n\r\n<p style=\"text-align:justify\">Wali Kota menambahkan disampaikan oleh WHO (World Health Organization) dan melalui IDI (Ikatan Dokter Indonesia) bahwa pada bulan Desember 2021 dan Januari 2022, akan ada gelombang ke 3 COVID-19, mudah-mudahan ini tidak terjadi di Indonesia, terutama di Kota Tebingtinggi.</p>\r\n\r\n<p style=\"text-align:justify\">&ldquo;Kita melakukan upaya pencegahan dengan vaksin dan menerapkan prokes 5M. Mengawinkan masker dengan vaksin, adalah upaya yang baik untuk dilaksanakan,&rdquo; tambah Wali Kota.</p>\r\n\r\n<p style=\"text-align:justify\">Sebelumnya, Wali Kota mengucapkan terimakasih kepada Kejari atas kesediaan sebagai saksi penyerahan dan menjadi tempat penyelenggaraan serta kepada MUI atas kerja kerasnya sehingga mendapat bantuan vaksin dan alat kesehatan dari Kemenkes RI.</p>\r\n\r\n<p style=\"text-align:justify\">Sementara Ketua MUI Kota Tebingtinggi, Drs. Akhyar Nasution mengatakan kegiatan ini merupakan kolaborasi antara Kejari dan MUI Kota Tebingtinggi serta merupakan salah satu kegiatan prioritas MUI Kota tahun 2021.</p>\r\n\r\n<p style=\"text-align:justify\">&ldquo;Hari ini MUI juga akan memberikan bantuan alkes dan bahan medis habis pakai yang merupakan hibah dari Pusat Krisis Kesehatan Kemenkes RI,&rdquo;ujarnya.</p>\r\n\r\n<p style=\"text-align:justify\">Lebih lanjut disampaikan Drs. Akhyar Nasution didampingi Sekretaris DR. Hasby H bahwa alokasi bantuan akan diberikan kepada fasilitas kesehatan, Madrasah serta sekolah-sekolah yang ada di Kota Tebing Tinggi.</p>\r\n\r\n<p style=\"text-align:justify\">Bantuan alat kesehatan yang diserahkan berupa APD/ hazmat cover all 5.000 pcs, gown (pakaian operasi) 1.500 pcs, masker KN95 3.500 pcs, masker bedah 150.000 pcs, oksigen konsentrator 15 unit, hand sanitizer 500 botol, hand scoon 500 pcs dan nurse cap 1.000 pcs.</p>\r\n\r\n<p style=\"text-align:justify\">&ldquo;Kami juga laporkan selain kegiatan penyerahan bantuan juga ada kegiatan vaksinasi lanjutan bagi pelajar dengan sasaran kurang lebih 450 peserta, berasal dari Madrasah dan sekolah umum yang ada di Kota Tebingtinggi,&rdquo; ujar Drs. Akhyar Nasution.</p>\r\n\r\n<p style=\"text-align:justify\">Kajari Tebing Tinggi Sundoro, Adi, SH, MH, menyampaikan rasa syukur Kota Tebing Tinggi mendapat dosis vaksin dan alat kesehatan serta berharap terbentuknya herd imunity di Kota Tebing Tinggi.</p>\r\n\r\n<p style=\"text-align:justify\">&ldquo;Kami Kejaksaan pada intinya siap dan tetap akan terus mensukseskan vaksinasi untuk mencegah penyebaran Covid-19. Dan saya harap momen vaksinasi yang ada bisa terus dilanjutkan, sehingga terbentuknya herd imunity di Kota Tebing Tinggi&rdquo; kata Kajari.</p>\r\n\r\n<p style=\"text-align:justify\">Acara turut dihadiri Sekdako Tebingtinggi&nbsp; Muhammad Dimiyathi, S.Sos, M.TP, Danramil 13/TT Kapt. Inf. Budiono, Wadanyon B Sat Brimob Polda Sumut AKP. Daud Pelawi, Dr. H. Abdul Rahim mewakili Ketua MUI Provinsi Sumut, Sekretaris MUI Kota Hasbie Ashshiddiqi, S.Pd.I., M.Si, perwakilan RS. Bhayangkara, RS. Dr. Kumpulan Pane, RS. Sri Pamela dan RS. Chevani, pimpinan OPD serta tokoh agama, masyarakat dan tamu undangan lainnya.</p>\r\n','tebing tinggi,ulama,vaksinasi,covid 19',0,0,'admin','2021-11-24 20:28:34','admin','2021-11-24 21:41:42'),
	(7,12,'2021-11-09','MUI Gelar Vaksinasi Pelajar di Tebing Tinggi','mui-gelar-vaksinasi-pelajar-di-tebing-tinggi','<p style=\"text-align:justify\">MUI Tebing Tinggi menggelar vaksinasi COVID-19 kepada 450 pelajar yang dirangkai dengan&nbsp;penyerahan alat kesehatan&nbsp; Kegiatan digelar dihalaman Kantor Kejari Tebing Tinggi, Senin (8/11).<br />\r\n<br />\r\nKetua MUI Tebing Tinggi Akhyar Nasution mengatakan kegiatan itu merupakan kolaborasi antara Kejari dan MUI Tebing Tinggi.&nbsp;<br />\r\n<br />\r\n&quot;Hari ini MUI Kota juga akan memberikan bantuan alat kesehatan dan bahan medis habis pakai yang merupakan hibah dari Pusat Krisis Kesehatan Kemenkes RI,&quot; katanya.</p>\r\n\r\n<p style=\"text-align:justify\">Lebih lanjut disampaikan H. Akhyar Nasution, alokasi bantuan akan diberikan kepada fasilitas kesehatan, Madrasah serta sekolah-sekolah yang ada di Kota Tebing Tinggi.&nbsp;<br />\r\n<br />\r\nBantuan alat kesehatan yang diserahkan berupa APD/ hazmat cover all 5.000 pcs, gown (pakaian operasi) 1.500 pcs.&nbsp;<br />\r\n<br />\r\nMasker KN95 3.500 pcs, masker bedah 150.000 pcs, oksigen konsentrator 15 unit, hand sanitizer 500 botol, hand scoon 500 pcs dan nurse cap 1.000 pcs.&nbsp;<br />\r\n<br />\r\nWali Kota Tebing Tinggi H.Umar Zunaidi Hasibuan, mengajak alim ulama, tokoh agama di Kota Tebing &nbsp;Tinggi aktif sosialisasikan program vaksinasi COVID-19 .&nbsp;<br />\r\n<br />\r\nIa berharap MUI Kota Tebing Tinggi agar menyampaikan kepada alim ulama dan tokoh agama agar memberi keyakinan terkait vaksinasi dan penerapan protokol kesehatan COVID-19 kepada masyarakat.&nbsp;<br />\r\n<br />\r\n&quot;Perlu kiranya MUI menyampaikan himbauan kepada para ustad dan ulama untuk terus disampaikan kepada ummat tentang vaksinasi karena sebagai upaya untuk kesehatan dan keselamatan masyarakat,&quot; katanya.<br />\r\n<br />\r\nSementara itu Kajari Tebing Tinggi Sundoro Adi, menyampaikan rasa syukur Kota Tebing Tinggi mendapat dosis vaksin dan alat kesehatan serta berharap terbentuknya herd imunity di Kota Tebing Tinggi.&nbsp;<br />\r\n<br />\r\n&quot;Kami Kejaksaan pada intinya siap dan tetap akan terus mensukseskan vaksinasi untuk mencegah penyebaran COVID-19. Diharapkan momen vaksinasi yang ada bisa terus dilanjutkan, sehingga terbentuknya herd imunity di Kota Tebing Tinggi,&quot; katanya.</p>\r\n','mui,vaksin,vaksinasi,pelajar,tebing tinggi',0,0,'admin','2021-11-24 20:32:53','admin','2021-11-24 23:07:47'),
	(19,8,'2021-11-22','MUI Kota Tebing Tinggi Menggalang Dana Bantuan untuk Korban Banjir di Kota Tebing Tinggi','mui-kota-tebing-tinggi-menggalang-dana-bantuan-untuk-korban-banjir-di-kota-tebing-tinggi','<p>&quot;Pengurus Majelis Ulama Indonesia Kota Tebing Tinggi harus memiliki rasa empati yang tinggi terhadap kondisi keadaan yang terjadi disekeliling&quot;. Hal tersebut disampaikan Sekretaris Umum Majelis Ulama Indonesia Kota Tebing Tinggi&nbsp;Dr. H. Muhammad HASBIE ASHSHIDDIQI, M.M., M.Si. di dampingi &nbsp;Dr. Hj. Afnizar, dan H. Ahmad Fauzan, S. Farm.Apt dan seluruh pengurus MUI &nbsp;khususnya komisi sosial dan bencana Kota Tebing Tinggi&nbsp;pada hari Senin (22 November 2021).</p>\r\n\r\n<p>Majelis Ulama Indonesia Kota Tebing Tinggi Indonesia Kota Tebing Tinggi melihat keadaan yang terjadi di wilayah Kota Tebing Tinggi pada tanggal 22 November 2021. Majelis Ulama Indonesia ke Tebing Tinggi membuka donasi bagi seluruh masyarakat Kota Tebing Tinggi dan khususnya pengurus di Kota Tebing Tinggi dalam hal penanganan banjir yang terjadi di wilayah Kota Tebing Tinggi pada hari Minggu 21 November 2021.</p>\r\n\r\n<p>Dalam Penggalan Donasi tersebut pada hari Senin 22 November 2021 Majelis Ulama Indonesia Berhasil menggalang Donasi sebesar 6.940.000. dalam waktu beberapa jam dan sebahagian dari penggalangan tersebut telah di salurkan dengan memberikan bantuan nasi bungkus dan kebutuhan makanan lainya kepada warga yang terkena dampak banjir khusus nya kecamatan Tebing Tinggi Kota.</p>\r\n\r\n<p>Selain dari pada &nbsp;itu MUI Kota Tebing Tinggi terus mengajak masyarakat dan pengurus MUI untuk terus melakukan donasi dalam mengatasi pasca terjadinya bencana banjir tersebut, saat ini MUI sedang mendata warga yang membutuhkan makanan dan kebutuhan lainya untuk selanjutnya dapat di salurkan kepada masyarakat yang terkena dampak bencana tersebut.</p>\r\n','mui,tebing tinggi,dana,banjir,korban',0,0,'admin','2021-11-24 20:59:17','admin','2021-11-24 23:17:07'),
	(20,8,'2021-11-17','Sosialisasi Pengelolaan Persampahan dan Perayaan World Cleanup Day (WCD) Kota Tebing Tinggi Tahun 2021','sosialisasi-pengelolaan-persampahan-dan-perayaan-world-cleanup-day-wcd-kota-tebing-tinggi-tahun-2021','<p>Pada hari Rabu, 17 November 2021, Dinas Lingkungan Hidup Kota Tebing Tinggi melaksanakan kegiatan&nbsp;&ldquo;Sosialisasi Pengelolaan Persampahan dan Perayaan World Cleanup Day (WCD) Kota Tebing Tinggi Tahun 2021&rdquo; yang dilaksanakan di Jl. Ahmad Yani Lingkungan VI Kel. Mandailing Kecamatan Kota Tebing Tinggi Kota.&nbsp;</p>\r\n\r\n<p>Kegiatan ini dibuka langsung oleh Sekretaris Daerah Kota Tebing Tinggi, Muhammad Dimiyathi, S.Sos, M.TP dan dihadiri oleh perwakilan dari beberapa SKPD terkait.</p>\r\n\r\n<p>Sesuai dengan tema WCD Tahun 2021 &ldquo;PILAH SAMPAH DARI RUMAH&rdquo;, Sekretaris Daerah Kota Tebing Tinggi, Muhammad Dimiyathi, S.Sos, M.T.P menekankan pentingnya peran serta masyarakat dalam menjaga kebersihan guna memelihara lingkungan hidup Kota Tebing Tinggi.</p>\r\n\r\n<p>Kepala Dinas Lingkungan Hidup Kota Tebing Tinggi, Dr. H. Muhammad Hasbie Ashshiddiqi, M.M, M.Si menyampaikan bahwa kegiatan dilaksanakan berdasarkan Surat Direktur Jenderal Pengelolaan Sampah dan Limbah dan Bahan Berbahaya dan Beracun Kementerian Lingkungan Hidup dan Kehutanan Nomor : S.362/PSLB3/PS/KLN.2/8/2021 tanggal 10 Agustus 2021 Perihal Dukungan Pelaksanaan Kegiatan WCD Indonesia 2021.&nbsp;</p>\r\n\r\n<p>&ldquo;World Cleanup Day adalah Gerakan sosial yang bertujuan mengajak dan mengedukasi masyarakat untuk memiliki kesadaran membersihkan, menjaga dan memelihara lingkungan mulai dari diri sendiri dan masyarakat,&rdquo;<br />\r\nPelaksanaan kegiatan ini meliputi beberapa rangkaian yaitu Penyerahan hadiah Lomba Lingkungan Hidup se-Kota Tebing Tinggi Tahun 2021; Penyerahan Piagam Penghargaan Kepada Sekolah yang Ikut Serta Sebagai 13 Juta Relawan Aksi World Cleanup Day Tahun 2021; Penyerahan Sepatu Boot, Sarung Tangan, Cakar, Masker kepada 10 (sepuluh) orang Pemulung sebagai Pahlawan Kebersihan; Penaburan benih/bibit ikan; dan melaksanakan Pilah Sampah dari Rumah.</p>\r\n\r\n<p>&ldquo;Pada Tahun 2022, Pemerintah Kota Tebing Tinggi mempunyai target pengurangan timbulan sampah sebesar 26% dengan cara pembatasan timbulan sampah, pendauran ulang sampah, pemanfaatan kembali sampah; serta 76% target penanganan sampah dengan cara pemilahan, pengumpulan, pengangkutan, pengolahan dan pemrosesan akhir,&rdquo; jelasnya.</p>\r\n\r\n<p>Melalui kegiatan pilah sampah dari 36 rumah tangga, diperoleh hasil pilah sampah dari rumah sebanyak 86,7 kg sampah organik dan 33,3 kg sampah anorganik.</p>\r\n\r\n<p>Diharapkan melalui kegiatan World Cleanup Day ini, dapat mengedukasi dan menumbuhkan minat masyarakat dalam pengelolaan sampah sehingga dapat merubah perilaku dan budaya untuk menjadi gerakan masyarakat peduli lingkungan dalam mewujudkan aksi untuk bumi yang bersih dan sehat.</p>\r\n','sampah,world,cleanup,day,tebing tinggi,wcd,2021',0,0,'admin','2021-11-24 21:25:03','admin','2021-11-24 21:25:03'),
	(23,12,'2021-11-09','MUI Kota Tebing Tinggi Salurkan 10.000 Dosis Vaksin Bagi Pelajar','mui-kota-tebing-tinggi-salurkan-10-000-dosis-vaksin-bagi-pelajar','<p>Wali&nbsp;Kota Tebing Tinggi Umar Zunaidi Hasibuan menerima audiensi Majelis Ulama Indonesia (MUI) Kota Tebing Tinggi. Audiensi tersebut dalam rangka pelaksanaan penyaluran bantuan alat kesehatan berupa alat pelindung diri (APD), masker, oksigen, hand sanitizer, hand scoon atau sarung tangan latex serta nurse cup dari Kementerian Kesehatan RI ke MUI Kota Tebing Tinggi dan bantuan vaksin Sinovac sebanyak 10 ribu dosis untuk dosis 1 dan dosis 2.</p>\r\n\r\n<p>Vaksinasi rencananya akan digelar pekan depan yaitu pada 8 November 2021.Sekretaris MUI Kota Tebing Tinggi Hasbie Ashshiddiqi menyampaikan bahwa MUI telah menerima 10 ribu vaksin Sinovac dan dalam waktu dekat akan melakukan vaksinasi pada pelajar Madrasah serta pelajar umum yang akan digelar di Kejaksaan Negeri Tebing Tinggi pada Senin (8/11/2021)</p>\r\n\r\n<p>&quot;Bersama Bapak Wali Kota, telah disepakati, pelaksanaan penyerahan dari MUI Kota ke Faskes Klinik Pratama dan Rumah Sakit di kantor Kejaksaan Negeri (Kejari) pada Senin, tanggal 8 November 2021, dengan disaksikan Pemerintah Kota Tebing tinggi dan pihak Kejari.</p>\r\n\r\n<p>Semoga bisa berjalan dengan lancar dan ridho Allah SWT,&quot; kata Hasbie di rumah dinas Wali Kota Tebing Tinggi, Selasa (2/11). Menanggapi hal tersebut, Wali Kota Tebing Tinggi Umar Zunaidi Hasibuan mengimbau MUI Kota Tebing Tinggi untuk berkoordinasi dengan Dinas Kesehatan terkait proses pelaksanaan penyaluran alat kesehatan serta persiapan vaksinasi.</p>\r\n\r\n<p>&quot;Saya minta koordinasi dengan Ibu Henny Plt. Kadis Kesehatan tentang pelaksanaan kegiatan ini dan membuat berita acaranya serta persiapannya. Senin depan (8/11/2021) bukan waktu yang lama. Kejaksaan Negeri dan Pemerintah Kota Tebing Tinggi sebagai saksi, karena yang menyerahkan bantuan dari MUI Kota,&quot; kata Umar Zunaidi. &quot;Hal untuk pelaksanaan vaksinasi bagi pelajar Madrasah dan pelajar umum, diupayakan ada tenda, kursi, dan snack. Kiranya kegiatan ini bisa berjalan dengan baik, lancar dan tetap menerapkan protokol kesehatan Covid-19.&quot;</p>\r\n','mui,tebing tinggi,vaksin,pelajar',0,0,'admin','2021-11-24 21:33:06','admin','2021-11-24 21:33:06'),
	(24,7,'2022-01-27','TEST','test','<p>DEV</p>\r\n',NULL,0,0,'admin','2022-01-27 10:25:14','admin','2022-01-27 10:25:14');

/*!40000 ALTER TABLE `_posts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_roles`;

CREATE TABLE `_roles` (
  `RoleID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `RoleName` varchar(50) NOT NULL,
  PRIMARY KEY (`RoleID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_roles` WRITE;
/*!40000 ALTER TABLE `_roles` DISABLE KEYS */;

INSERT INTO `_roles` (`RoleID`, `RoleName`)
VALUES
	(1,'Administrator'),
	(2,'Penulis');

/*!40000 ALTER TABLE `_roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_settings`;

CREATE TABLE `_settings` (
  `SettingID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `SettingLabel` varchar(50) NOT NULL,
  `SettingName` varchar(50) NOT NULL,
  `SettingValue` text NOT NULL,
  PRIMARY KEY (`SettingID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_settings` WRITE;
/*!40000 ALTER TABLE `_settings` DISABLE KEYS */;

INSERT INTO `_settings` (`SettingID`, `SettingLabel`, `SettingName`, `SettingValue`)
VALUES
	(1,'SETTING_WEB_NAME','SETTING_WEB_NAME','LIHATNEWS'),
	(2,'SETTING_WEB_DESC','SETTING_WEB_DESC','Lumbung Informasi Harian Aktual Terpercaya'),
	(3,'SETTING_WEB_DISQUS_URL','SETTING_WEB_DISQUS_URL','https://general-9.disqus.com/embed.js'),
	(4,'SETTING_ORG_NAME','SETTING_ORG_NAME','-'),
	(5,'SETTING_ORG_ADDRESS','SETTING_ORG_ADDRESS','-'),
	(6,'SETTING_ORG_LAT','SETTING_ORG_LAT',''),
	(7,'SETTING_ORG_LONG','SETTING_ORG_LONG',''),
	(8,'SETTING_ORG_PHONE','SETTING_ORG_PHONE','-'),
	(9,'SETTING_ORG_FAX','SETTING_ORG_FAX','-'),
	(10,'SETTING_ORG_MAIL','SETTING_ORG_MAIL','-'),
	(11,'SETTING_WEB_API_FOOTERLINK','SETTING_WEB_API_FOOTERLINK','-'),
	(12,'SETTING_WEB_LOGO','SETTING_WEB_LOGO','logo.png'),
	(13,'SETTING_WEB_SKIN_CLASS','SETTING_WEB_SKIN_CLASS','skin-green-light'),
	(14,'SETTING_WEB_PRELOADER','SETTING_WEB_PRELOADER','main.gif'),
	(15,'SETTING_WEB_VERSION','SETTING_WEB_VERSION','1.0'),
	(16,'SETTING_ORG_REGION','SETTING_ORG_REGION','-');

/*!40000 ALTER TABLE `_settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _userinformation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_userinformation`;

CREATE TABLE `_userinformation` (
  `UserName` varchar(50) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `CompanyID` varchar(200) DEFAULT NULL,
  `Name` varchar(250) DEFAULT NULL,
  `IdentityNo` varchar(50) DEFAULT NULL,
  `BirthDate` date DEFAULT NULL,
  `ReligionID` int(10) DEFAULT NULL,
  `Gender` tinyint(1) DEFAULT NULL,
  `Address` text,
  `PhoneNumber` varchar(50) DEFAULT NULL,
  `EducationID` int(10) DEFAULT NULL,
  `UniversityName` varchar(50) DEFAULT NULL,
  `FacultyName` varchar(50) DEFAULT NULL,
  `MajorName` varchar(50) DEFAULT NULL,
  `IsGraduated` tinyint(1) NOT NULL DEFAULT '0',
  `GraduatedDate` date DEFAULT NULL,
  `YearOfExperience` int(10) DEFAULT NULL,
  `RecentPosition` varchar(250) DEFAULT NULL,
  `RecentSalary` double DEFAULT NULL,
  `ExpectedSalary` double DEFAULT NULL,
  `CVFilename` varchar(250) DEFAULT NULL,
  `ImageFilename` varchar(250) DEFAULT NULL,
  `RegisteredDate` date DEFAULT NULL,
  PRIMARY KEY (`UserName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_userinformation` WRITE;
/*!40000 ALTER TABLE `_userinformation` DISABLE KEYS */;

INSERT INTO `_userinformation` (`UserName`, `Email`, `CompanyID`, `Name`, `IdentityNo`, `BirthDate`, `ReligionID`, `Gender`, `Address`, `PhoneNumber`, `EducationID`, `UniversityName`, `FacultyName`, `MajorName`, `IsGraduated`, `GraduatedDate`, `YearOfExperience`, `RecentPosition`, `RecentSalary`, `ExpectedSalary`, `CVFilename`, `ImageFilename`, `RegisteredDate`)
VALUES
	('admin','yoelrolas@gmail.com',NULL,'Administrator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-17');

/*!40000 ALTER TABLE `_userinformation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_users`;

CREATE TABLE `_users` (
  `UserName` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `RoleID` int(10) unsigned NOT NULL,
  `IsSuspend` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `LastLogin` datetime DEFAULT NULL,
  `LastLoginIP` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`UserName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_users` WRITE;
/*!40000 ALTER TABLE `_users` DISABLE KEYS */;

INSERT INTO `_users` (`UserName`, `Password`, `RoleID`, `IsSuspend`, `LastLogin`, `LastLoginIP`)
VALUES
	('admin','3798e989b41b858040b8b69aa6f2ce90',1,0,'2022-01-27 10:23:29','::1');

/*!40000 ALTER TABLE `_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table logs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `logs`;

CREATE TABLE `logs` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `PostID` bigint(10) DEFAULT NULL,
  `LogURL` text NOT NULL,
  `LogRemarks` text,
  `LogTimestamp` datetime NOT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `logs` WRITE;
/*!40000 ALTER TABLE `logs` DISABLE KEYS */;

INSERT INTO `logs` (`Uniq`, `PostID`, `LogURL`, `LogRemarks`, `LogTimestamp`)
VALUES
	(1,23,'http://localhost/web-lihatnews/site/home/page/mui-kota-tebing-tinggi-salurkan-10-000-dosis-vaksin-bagi-pelajar.jsp',NULL,'2021-11-25 17:35:33'),
	(2,23,'http://localhost/web-lihatnews/site/home/page/mui-kota-tebing-tinggi-salurkan-10-000-dosis-vaksin-bagi-pelajar.jsp',NULL,'2021-11-25 17:35:37'),
	(3,7,'http://localhost/web-lihatnews/site/home/page/mui-gelar-vaksinasi-pelajar-di-tebing-tinggi.jsp',NULL,'2021-11-25 17:35:47'),
	(4,6,'http://localhost/web-lihatnews/site/home/page/walikota-tebing-tinggi-ajak-alim-ulama-aktif-sosialisasi-program-vaksinasi-covid-19.jsp',NULL,'2021-11-25 17:35:55'),
	(5,7,'http://localhost/web-lihatnews/site/home/page/mui-gelar-vaksinasi-pelajar-di-tebing-tinggi.jsp',NULL,'2021-11-25 17:36:03'),
	(6,23,'http://localhost/web-lihatnews/site/home/page/mui-kota-tebing-tinggi-salurkan-10-000-dosis-vaksin-bagi-pelajar.jsp',NULL,'2021-11-25 17:36:10'),
	(7,24,'http://localhost/web-lihatnews/site/home/page/test.jsp',NULL,'2022-01-27 10:25:17');

/*!40000 ALTER TABLE `logs` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
